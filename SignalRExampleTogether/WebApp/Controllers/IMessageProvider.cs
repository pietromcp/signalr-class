﻿namespace WebApp.Controllers
{
    public interface IMessageProvider
    {
        string BuildMessage(string to);
    }
}