﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using WebApp.Controllers;

namespace WebApp.Installers
{
    public class ServicesInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IMessageProvider>().ImplementedBy<BasicMessageProvider>()
                .LifestyleSingleton());
        }
    }
}