﻿using Owin;
using Microsoft.Owin.Cors;
using System.Web.Http;

namespace ServerSentEventsHelloServer
{
    public class Startup
    {
        // This code configures Web API. The Startup class is specified as a type
        // parameter in the WebApp.Start method.
        public void Configuration(IAppBuilder appBuilder)
        {
            // Configure Web API for self-host. 
            HttpConfiguration config = new HttpConfiguration();
            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);
            config.MapHttpAttributeRoutes();
            appBuilder.UseCors(CorsOptions.AllowAll);
            config.EnsureInitialized();

            appBuilder.UseWebApi(config);
        }
    }
}
