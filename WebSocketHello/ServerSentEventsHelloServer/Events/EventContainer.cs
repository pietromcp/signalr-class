﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;

namespace ServerSentEventsHelloServer.Events
{
    internal class EventContainer
    {
        private static object SyncRoot = new object();

        private static IList<string> Events = new List<string>();

        public static void Enqueue(string evt)
        {            
            lock (SyncRoot) {
                Events.Add(evt);
                Monitor.PulseAll(SyncRoot);
            }
        }

        public static IList<string> GetEvents(TimeSpan timeout) {
            lock (SyncRoot) {
                if (!Events.Any()) {
                    Monitor.Wait(SyncRoot, timeout);
                }
                var result = new List<string>(Events);
                Events.Clear();
                return result;
            }
        }
    }
}
