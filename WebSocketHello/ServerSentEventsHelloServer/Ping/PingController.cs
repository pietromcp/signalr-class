﻿using System.Collections.Generic;
using System.Web.Http;

namespace ServerSentEventsHelloServer.Ping
{
    [RoutePrefix("api/ping")]
    public class PingController : ApiController
    {
        [Route("")]
        public IEnumerable<string> GetValues() {
            return new[] { "foo", "bar", "buzz"};
        }
    }
}
