﻿using System;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace WebSocketHelloServer
{
    class Program {
        public static void Main(string[] args) {
            var wssv = new WebSocketServer("ws://localhost:2345");
            wssv.AddWebSocketService<HelloService>("/hello");
            wssv.Start();
            Console.WriteLine("Socket-side up and running. Press any key to stop...");
            Console.ReadKey(true);
            wssv.Stop();
        }
    }

    public class HelloService : WebSocketBehavior {
        protected override void OnMessage(MessageEventArgs e) {
            string inputMessage = e.Data;
            Console.WriteLine($"Client says: {inputMessage}");
            string responseMsg = $"Hello, {e.Data}";
            Send(responseMsg);
        }
    }
}
