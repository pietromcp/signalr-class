﻿using Microsoft.AspNet.SignalR;

namespace SignalRHelloServer.Money
{
    public class MoneyHub : Hub {
        private const double LIRE_PER_EURO = 1936.27;

        public void LireToEuro(double amount) {
            double euroAmount = amount / LIRE_PER_EURO;
            Clients.Caller.ShowConversion(new { From = "£", To = "€", Amount = amount, ConvertedAmount = euroAmount });
        }

        public void EuroToLire(double amount)
        {
            double lireAmount = amount * LIRE_PER_EURO;
            Clients.Caller.ShowConversion(new { To = "£", From = "€", Amount = amount, ConvertedAmount = lireAmount });
        }
    }
}
