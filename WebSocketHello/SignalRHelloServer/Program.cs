﻿using Microsoft.Owin.Hosting;
using System.Configuration;
using System;

namespace ServerSentEventsHelloServer
{
    class Program
    {
        static void Main()
        {
            string baseAddress = ConfigurationManager.AppSettings["BindUri"];
            var host = WebApp.Start<Startup>(url: baseAddress);
            Console.WriteLine("WebApi started. Press any key to exit");            
            Console.ReadKey();
        }
    }
}
