﻿using System;
using System.Threading;
using WebSocketSharp;

namespace WebSocketHelloClient {
    class Program     {
        static void Main(string[] args)         {
            using (var ws = new WebSocket("ws://localhost:2345/hello"))             {
                ws.OnMessage += HandleIncomingMessage;                    

                ws.Connect();
                ws.Send("World");
                Thread.Sleep(250);
                ws.Send("Pietro");
                string to = null;
                bool go = true;
                while (to != "exit") {
                    Console.WriteLine("Please insert a name (exit to end)");
                    to = Console.ReadLine();
                    if (to != "exit") {
                        ws.Send(to);
                    } else {
                        go = false;
                    }                    
                }
            }
        }

        static void HandleIncomingMessage(object sender, MessageEventArgs e) {
            Console.WriteLine("Server says: " + e.Data);
        }
    }
}
